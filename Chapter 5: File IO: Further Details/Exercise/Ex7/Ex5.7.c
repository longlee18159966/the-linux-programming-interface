/*
 * =====================================================================================
 *
 *       Filename:  Ex5.7.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30/09/2021 21:06:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */


/* Implement readv() and writev() using read(), write() 
 * and suitable functions from the mallo package
 */

#include <stdlib.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include "lib/tlpi_hdr.h"
#include "string.h"
#include <stdbool.h>
#include <error.h>


ssize_t readv_t2 (int fd, struct iovec *iov_t, int iovcnt);
ssize_t readv_t (int fd, const struct iovec *, int iovcnt);
ssize_t writev_t (int fd, struct iovec *iov, int iovcnt);

int main (int argc, char *argv[])
{

    int fd;

    struct iovec iov[3];
    struct stat myStruct; /* First buffer */
    int x; /* Second buffer */
    #define STR_SIZE 20
    int str[STR_SIZE]; /* Third buffer */
    ssize_t numRead, totRequired;
    if (argc != 3 || strcmp(argv[1], "--help") == 0)
    usageErr("%s file-source file-des\n", argv[0]);
    fd = open(argv[1], O_RDONLY);
    int fd1 = open(argv[2], O_CREAT | O_RDWR | O_APPEND, S_IRWXU | S_IRWXG | S_IRWXO);
    if (fd == -1)
        errExit("open");
    totRequired = 0;
    iov[2].iov_base = &myStruct;
    iov[2].iov_len = sizeof(struct stat);
    totRequired += iov[0].iov_len;
    iov[1].iov_base = &x;
    iov[1].iov_len = sizeof(x);
    totRequired += iov[1].iov_len;
    iov[0].iov_base = str;
    iov[0].iov_len = STR_SIZE;
    totRequired += iov[2].iov_len;

    //numRead = readv(fd, iov, 3);


    // Ham nay dang bi loi
    //numRead = readv_t(fd, iov, 3);


    numRead = readv_t2(fd, iov, 3);

    printf("\n\n********************* IN MAIN: *******************\n\n");
    printf("%s", (char *)iov[0].iov_base);
    printf("%s", (char *)iov[1].iov_base);
    printf("%s", (char *)iov[2].iov_base);
    printf("\n************* address: %p ***************\n\n", iov);



    if (numRead == -1)
        errExit("readv");
    if (numRead < totRequired)
        printf("Read fewer bytes than requested\n");
    printf("total bytes requested: %ld; bytes read: %ld\n", (long) totRequired, (long) numRead);
    writev(fd1, iov, 3);
    exit(EXIT_SUCCESS);
}



ssize_t readv_t (int fd, const struct iovec *iov, int iovcnt)
{
    struct iovec *iov_t = (struct iovec*) malloc(iovcnt * sizeof(struct iovec));
    ssize_t NumRead = 0;
    for (int i = 0; i < iovcnt; ++i)
        iov_t[i] = iov[i];


    for (int i = 0; i < iovcnt; ++i)
    {
        printf("iov_t_len[%d]: %ld\n", i, iov_t[i].iov_len);
        printf("iov_t_base[%d]: %p\n", i, iov_t[i].iov_base);
    }


    for (int i = 0; i < iovcnt; ++i)
    {
        printf("iov_len[%d]: %ld\n", i, iov[i].iov_len);
        printf("iov_base[%d]: %p\n", i, iov[i].iov_base);
    }



    for (int i = 0; i < iovcnt; ++i)
    {
        iov_t[i].iov_base = malloc(iov_t[i].iov_len);
        NumRead += read(fd, iov_t[i].iov_base, iov_t[i].iov_len);
    }
    iov = iov_t;
    printf("iov: %p\tiov_t: %p\n", iov, iov_t);
    printf("%s", (char *)iov_t[0].iov_base);
    printf("%s", (char *)iov_t[1].iov_base);
    printf("%s", (char *)iov_t[2].iov_base);

    putchar('\n');

    printf("%s", (char *)iov[0].iov_base);
    printf("%s", (char *)iov[1].iov_base);
    printf("%s", (char *)iov[2].iov_base);


    return NumRead;
}

ssize_t readv_t2 (int fd, struct iovec *iov_t, int iovcnt)
{
    ssize_t NumRead = 0;
    for (int i = 0; i < iovcnt; ++i)
    {
        iov_t[i].iov_base = malloc(iov_t[i].iov_len);
        NumRead += read(fd, iov_t[i].iov_base, iov_t[i].iov_len);
    }

    return NumRead;
}
