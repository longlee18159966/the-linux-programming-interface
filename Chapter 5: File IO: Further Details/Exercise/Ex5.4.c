/*
 * =====================================================================================
 *
 *       Filename:  Ex5.4.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30/09/2021 19:38:45
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

/*
 * Implement dup() and dup2() using fcntl() and, where necessary close(). 
 * (You may ignore the fact that dup2() and fcntl() return different errno values for some error case)
 * For dup2(), remember to handle the special case where oldfd equal newfd. In this case,
 * you should check whether oldfd is valid, which can be done by, for example, checking
 * if fcntl(oldfd, F_GETFL) succeeds. If oldfd is not valid, then the function should
 * return -1 with errno set to EBADF
 */
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include "lib/tlpi_hdr.h"
#include "string.h"
#include <stdbool.h>
#include <error.h>

int main (int argc, char *argv[])
{
    if (argc != 2)
        usageErr("%s file-name\n", argv[0]);
    
    int oldfd, newfd1, newfd2;
    if ((oldfd = open(argv[1], O_CREAT | O_RDWR, S_IRWXU | S_IRWXG | S_IRWXO)) == -1)
        errExit("open\n");

    /* for dup() */
    close(4);
    newfd1 = fcntl(oldfd, F_DUPFD, 0);
    printf("%d\n", newfd1);

    /* for dup2() */
    if (fcntl(oldfd, F_GETFL) == -1)
    {
        strerror(errno);
        errExit("invalid oldfd\n");
    }
    close(10);
    newfd2 = fcntl(oldfd, F_DUPFD, 10);
    printf("%d\n", newfd2);

    close(oldfd);
    close(newfd1);
    close(newfd2);

    exit(EXIT_SUCCESS);
}























