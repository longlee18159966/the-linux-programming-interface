
/* 
 * Write a program that opens an existing file for writing with the O_APPEND flag
 * and then seeks to the beginning of the file before writing some data.
 * Where does the data appear in the file? Why?
 */


#include <stdlib.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include "lib/tlpi_hdr.h"
#include "string.h"

int main (int argc, char *argv[])
{
    if (argc != 2)
        usageErr("%s file-name\n", argv[0]);

    int fd;
    if ((fd = open(argv[1], O_RDWR | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)) == -1)
        errExit("can't open %s\n", argv[1]);
    char buffer[100] = "This battle will be my masterpiece\n";
    char print[100];
    lseek(fd, 0, SEEK_SET);
    write(fd, (char *)buffer, strlen(buffer));
    lseek(fd, 0, SEEK_SET);
    read(fd, (char *)print, 9);
    puts(print);
    if (close(fd) == -1)
        errExit("close file\n");

    exit(EXIT_SUCCESS);
}
