/*
 * =====================================================================================
 *
 *       Filename:  Ex5.3.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30/09/2021 15:00:35
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

/* 
 * This exercise is designed to demonstrate why the atomicity guaranteed by opening
 * a file with the O_APPEND flag is necessary. Write a program that takes up to three
 * command-line arguments:
 *
 *  $ program filename num-bytes [x]
 *
 * This file should open the specified filename (creating it if necessary) and append
 * num-bytes bytes to the file by using write() to write a byte at a time. By default, 
 * the program should open the file with the O_APPEND flag, but if a third command-line
 * argument (x) is supplied, then the O_APPEND flag should be omitted, and instead the
 * program should perform an lseek(fd, 0, SEEK_END) call before each write(). Run two
 * instances of this program at the same time without the x argument to write 1 million
 * bytes to the same file:
 *
 *  $ program f1 1000000 & program f1 1000000
 *
 * Repeat the same steps, writing to a different file, but this time specifying the
 * x argument:
 *
 *  $ program f2 1000000 x & program f2 1000000 x
 *
 * List the sizes of the files f1 and f2 using ls -l and explain the difference
 */


/* 
 * Explain the difference f1 size and f2 size: 
 * o truong hop f1, flag O_APPEND dam bao viec dich con tro file 
 * ve cuoi file luon duoc thuc thi xong, sau no moi write dan den file co kich thuoc lon hon
 *
 * o truong hop f2, fseek() khong thuc hien atomicity, trong khi lseek() cua lan thuc thi dau tien
 * dang dich con tro den cuoi file thi lseek() cua lan thuc thi thu 2 dien ra => xay ra viec ghi de
 * du lieu => kich thuoc f2 nho hon f1
 */

#include <stdlib.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include "lib/tlpi_hdr.h"
#include "string.h"
#include <stdbool.h>


int main (int argc, char *argv[])
{
    int fd;
    bool CheckX = false;
    long num_byte = atoll(argv[2]);
    char buffer[num_byte];
    for (long i = 0; i < num_byte; ++i)
        buffer[i] = i;
    if (argc == 3)
    {
        if ((fd = open(argv[1], O_RDWR | O_APPEND | O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO)) == -1)
        {
            errExit("open\n");
        }
    }
    else if (argc == 4)
    {
        if ((fd = open(argv[1], O_RDWR | O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO)) == -1)
        {
            errExit("open\n");
        }
        CheckX = true;
    }
    else 
    {
        usageErr("%s file-name num-bytes [x]\n", argv[0]);
    }

    if (CheckX == false)
    {
        write(fd, buffer, num_byte);
    }
    else
    {
        lseek(fd, 0, SEEK_END);
        write(fd, buffer, num_byte);
    }




    if (close(fd) == -1)
        errExit("close\n");
    exit(EXIT_SUCCESS);
} 
