/*
 * =====================================================================================
 *
 *       Filename:  Ex5.6.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30/09/2021 20:57:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

/*
 * After each of the calls to write() in the following code, explain what the content of
 * the output file would be, and why:
 * fd1 = open(file, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
 * fd2 = dup(fd1);
 * fd3 = open(file, O_RDWR);
 * write(fd1, "Hello,", 6);
 * write(fd2, "world", 6);
 * lseek(fd2, 0, SEEK_SET);
 * write(fd1, "HELLO,", 6);
 * write(fd3, "Gidday", 6);
 */




#include <stdlib.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include "lib/tlpi_hdr.h"
#include "string.h"
#include <stdbool.h>
#include <error.h>

int main (int argc, char *argv[])
{
    int fd1, fd2, fd3;
    fd1 = open(argv[1], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    fd2 = dup(fd1);
    fd3 = open(argv[1], O_RDWR);
    write(fd1, "Hello,", 6);
    write(fd2, "world\n", 6);
    lseek(fd2, 0, SEEK_SET);
    write(fd1, "HELLO,", 6);
    write(fd3, "Gidday", 6);

}
