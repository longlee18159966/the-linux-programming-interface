/*
 * =====================================================================================
 *
 *       Filename:  Ex5.5.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30/09/2021 20:32:35
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

/* 
 * Write a program to verify that duplicated file descriptors
 * share a file offset value and open file status flag
 */

#include <stdlib.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include "lib/tlpi_hdr.h"
#include "string.h"
#include <stdbool.h>
#include <error.h>

int main (int argc, char *argv[])
{
    if (argc != 2)
        usageErr("%s file-name\n", argv[0]);

    int oldfd, newfd;
    if ((oldfd = open(argv[1], O_WRONLY |O_APPEND, S_IRWXU | S_IRWXG)) == -1)
    {
        errExit("open");
    }
    off_t offset1, offset2;

    lseek(oldfd, 45, SEEK_SET);

    newfd = dup2(oldfd, 4);
    offset1 = lseek(oldfd, 0, SEEK_CUR);
    offset2 = lseek(newfd, 0, SEEK_CUR);
    printf("offset1: %ld\noffset2: %ld\n", offset1, offset2);

    int flag1, flag2;
    flag1 = fcntl(oldfd, F_GETFL);
    flag2 = fcntl(newfd, F_GETFL);

    printf("flag1: %d\nflag2: %d\n", flag1, flag2);
    close(oldfd);
    close(newfd);

    exit(EXIT_SUCCESS);
}
