/*
 * =====================================================================================
 *
 *       Filename:  Listing5.1.c
 *
 *    Description: :q 
 *
 *        Version:  1.0
 *        Created:  29/09/2021 22:57:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include "lib/tlpi_hdr.h"


int main (int argc, char *argv[])
{
    int fd;
    fd = open(argv[1], O_WRONLY);
    if (fd != -1)
    {
        printf("[PID %ld] File \"%s\" already exists\n", (long)getpid(), argv[1]);
        close(fd);
    }
    else
    {
        if (errno != ENOENT)
            errExit("open");
        else
        {
            printf("\n[PID %ld] File \"%s\" doesn't exist yet\n", (long)getpid(), argv[1]);
            if (argc > 2)
            {
                sleep(15);
                printf("\n[PID %ld] Done sleeping\n", (long)getpid());
            }
            /*  WINDOW for FAILURE */
            fd = open(argv[1], O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
            if (fd == -1)
                errExit("open");
            printf("\n[PID %ld] Create file \"%s\" already exclusively\n", (long)getpid(), argv[1]);
        }
    }

}


