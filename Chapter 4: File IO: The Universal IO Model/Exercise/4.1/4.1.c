/*  
 * The tee command reads its standard input until end-of-file, writing a copy of 
 * the input to standard output and to the file named in its command-line argument
 * Implement tee using I/O system calls. By default, tee overwrites any existing
 * file width the given name. Implement the -a command-line option (tee -a file),
 * which causes tee to append text to the end of a file if it already exists
 */

#include <sys/stat.h>
#include <fcntl.h>
#include "tlpi_hdr.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>


#define BUFFER_SIZE     128
static bool flags = false;
int opt;

int main (int argc, char *argv[])
{
    if ((argc != 2) && (argc != 3))
        usageErr("%s file <-a>\n", argv[0]);

    int fd;
    char buffer[BUFFER_SIZE];
    ssize_t numRead;

    while ((opt = getopt(argc, argv, ":a")) != -1)
    {
        switch (opt)
        {
            case 'a':
                flags = true;
                break;
            defalut:
                flags = false;
                break;
        }
    }

    if (flags)
    {
        fd = open(argv[1], O_RDWR | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);     /* rw- rw- rw- */
        printf("flags true\n");
    }
    else
    {
        fd = open(argv[1], O_RDWR | O_TRUNC | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);     /* rw- rw- rw- */
        printf("flags false\n");
    }

    while ((numRead = read(STDIN_FILENO, buffer, (ssize_t) BUFFER_SIZE)) != -1)
    {
        if (write(STDOUT_FILENO, buffer, numRead) == -1)
            errExit("Can't write to stdout\n");
        if (write(fd, buffer, numRead)  == -1)
            errExit("Can't write to %s\n", argv[1]);
    }
    
    close(fd);
    exit(EXIT_SUCCESS);
}

