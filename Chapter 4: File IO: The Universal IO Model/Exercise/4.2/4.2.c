/* 
 * Write a program like cp command that, when used to copy a 
 * regular file that contains holes (sequences of null bytes), 
 * also creates corresponding holes in the target file
 */


#include <sys/stat.h>
#include <fcntl.h>
#include "tlpi_hdr.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#define BUFFER_SIZE 128

int main (int argc, char *argv[])
{
    int source;
    int des;
    int count = 0;
    char buffer[BUFFER_SIZE];
    ssize_t numRead;

    if (argc != 3)
        usageErr("%s: SourceFile DestinationFile\n", argv[0]);
    if ((source = open(argv[1], O_RDONLY, S_IRUSR)) == -1)
        errExit("Can't open file %s\n", argv[1]);
    if ((des = open(argv[2], O_RDWR | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)) == -1)
        errExit("Can't open file %s\n", argv[2]);

    while ((numRead = read(source, buffer, BUFFER_SIZE)) > 0)
    {
        ++count;
        if (write(des, buffer, numRead) != numRead)
            fatal("Couldn't write whole buffer");
        if (write(STDOUT_FILENO, buffer, numRead) != numRead)
            fatal("Couldn't write whole to stdout");

    }
    printf("\ncount = %d\n", count);

    if (close(source) == -1)
        errExit("close input file");
    if (close(des) == -1)
        errExit("close output file");

    exit(EXIT_SUCCESS);
}
