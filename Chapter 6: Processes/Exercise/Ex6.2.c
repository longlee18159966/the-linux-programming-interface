
/* 
 * Write a program to see what happens if we try to 
 * longjmp() into a function that has already returned 
 */

/* 
 * longjmp() to returned function causes segmentation fault (core dumped) error
 *
 * Explain: setjmp() saves various information about the calling enviroment 
 * (typically, the stack pointer, the instruction pointer) in the buffer env
 * for later use by longjmp(). When setjmp() is called in returned function,
 * env is empty so next longjmp() doesn't know target to jump to. As a result,
 * segmentation fault (core dumped) occur because bad pointer
 */

#include <stdio.h>
#include <setjmp.h>
#include <stdlib.h>

static jmp_buf env;

static void f1 (void)
{
    printf("\nFunction 1\n");
    
    return ;

    setjmp(env);
}

static void f2 (void)
{
    printf("\nFunction 2\n");

    longjmp(env, 1);

    return ;
}

int main (void)
{
    f1();
    f2();

    exit(EXIT_SUCCESS);
}
