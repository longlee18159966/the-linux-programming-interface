/* 
 * Implement setenv() and unsetenv() using getenv(), putenv(), 
 * and, where necessary, code that directly modifies environ.
 * Your version of unsetenv() should check to see whether 
 * there are multiple definitions of an environment variable, and 
 * remove them all (which is what the glibc version of unsetenv() does).
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lib/tlpi_hdr.h"
#include <stdbool.h>


extern char **environ;

/*
 * true mean two string are same
 * false mean not
 */
bool string_compare (const char *s1, const char *s2);

int setenv_t (const char *name, const char *value, int overwrite);

void display_environment_list (void);

bool check_env_name (const char *s, const char *name);

int unsetenv_t (const char *name);

int main (int argc, char *argv[])
{
    #define a
    #ifndef a
    setenv("long", "hello", 0);
    setenv("long",argv[1], 0);
    display_environment_list();
    unsetenv_t("long");
    display_environment_list();
    #endif

    #ifdef a
    setenv_t("long", "hello", 0);
    setenv_t("long", argv[1], 0);
    display_environment_list();
    unsetenv_t("long");
    display_environment_list();

    #endif

    
    exit(EXIT_SUCCESS);
}



int unsetenv_t (const char *name)
{
    for (int count = 0; environ[count] != NULL; ++count)
    {
        if (check_env_name(environ[count], name) == true)
        {
            int j;
            for (j = count; environ[j] != NULL; ++j)
                environ[j] = environ[j+1];
            environ[j+1] = NULL;
        }
    }

    return 0;
}


void display_environment_list (void)
{
    for (char **ep = environ; *ep != NULL; ++ep)
        puts(*ep);
}


bool check_env_name (const char *s, const char *name)
{
    /* Get enviroment name */
    int i;
    char buffer[100];
    for (i = 0; i < strlen(s); ++i)
    {
        if (s[i] == '=')
            break;
        buffer[i] = s[i];
    }
    buffer[i] = '\0';

    return string_compare(buffer, name);
}


int setenv_t (const char *name, const char *value, int overwrite)
{
    int ret_val = 0;
    /* if file doesn't exist or overwrite mode on (overwrite != 0) */
    if ((getenv(name) == NULL) || (overwrite != 0))
    {
        int size = strlen(name) + strlen(value) + 2;
        char *s = (char *) malloc(size * sizeof(char));
        strcat(s, name);
        strcat(s, "=");
        strcat(s, value);
        ret_val = putenv(s);
    }
    else
        ret_val = 0;

    return ret_val;
}

/*
 * true mean two string are same
 * false mean not
 */
bool string_compare (const char *s1, const char *s2)
{
    if (strlen(s1) != strlen(s2))
        return false;
    for (int i = 0; i <= strlen(s1); ++i)
        if (s1[i] != s2[i])
            return false;

    return true;
}


